# Super Hexagon Keyboard Hack

This repo contains the code from my [YouTube video][yt] about hacking Super Hexagon using Ghidra. Included are a Python script that attempts to read Super Hexagon's memory to show a debugging display and a [ckb-next][ckb] animation plugin written in C.

A few notes:

- As written, this code will probably only work on the 64-bit Linux build of the Steam version of Super Hexagon. It shouldn't be too hard for you to adapt it to other versions/operating systems if you'd like.
- Both programs hard-code Super Hexagon's location to a path in my home directory, so they probably won't work for you unless you edit that.
- To run Super Hexagon outside Steam, you may need to create a file called `steam_appid.txt` in `steamapps/common/Super Hexagon` containing the number 221640.
- The ckb-next animation launches Super Hexagon when you activate the animation. It might not work if you already have Super Hexagon open. Super Hexagon runs as a child process of the keyboard driver, which is... probably not fantastic. (Better-written RGB keyboard tools, like [Aurora][pa], usually elevate to admin privileges so they can read the memory of processes that aren't their children.)
- To run the Python program, you'll need [Pygame][pygame] and [pyelftools][].
- To use the ckb-next driver, copy the contents of `ckb-next-plugin` to a folder called `superhexagon` in `ckb-next/src/animations`, add `SUPERHEXAGON` to the list of animations in `ckb-next/src/animations/CMakeLists.txt`, and set the `WITH_SUPERHEXAGON` CMake variable when compiling ckb-next. The animation stops working if you get to the game's ending. Not really sure why.

Good luck, and happy hacking!

[yt]: https://www.youtube.com/watch?v=kSSxJTpoLGo
[ckb]: https://github.com/ckb-next/ckb-next
[pa]: https://www.project-aurora.com/
[pygame]: https://pygame.org
[pyelftools]: https://github.com/eliben/pyelftools

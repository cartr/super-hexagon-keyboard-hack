#!/usr/bin/env python

import os
SUPER_HEXAGON_PATH = '/home/carter/.steam/steam/steamapps/common/Super Hexagon'
SUPER_HEXAGON_LIBS = os.path.join(SUPER_HEXAGON_PATH, 'x86_64')
SUPER_HEXAGON_BINARY = os.path.join(SUPER_HEXAGON_LIBS, 'superhexagon.x86_64')

print(SUPER_HEXAGON_BINARY)

# Step 1: Locate `SuperHexagonApp` variable

from elftools.elf.elffile import ELFFile

with open(SUPER_HEXAGON_BINARY) as f:
    elf = ELFFile(f)
    SuperHexagonApp = elf.get_section_by_name('.symtab').get_symbol_by_name('SuperHexagonApp')[0]['st_value']
    print SuperHexagonApp

# Step 2: Start Super Hexagon and get its pid

import subprocess
import os

os.environ['LD_LIBRARY_PATH'] = SUPER_HEXAGON_LIBS

superHexagonProcess = subprocess.Popen([SUPER_HEXAGON_BINARY], cwd=SUPER_HEXAGON_PATH, env=os.environ)
pid = superHexagonProcess.pid

print pid

# Step 3: read colors from Super Hexagon's memory, display them

import math, struct

pointerStruct = struct.Struct('Q')
intStruct = struct.Struct('i')
doubleStruct = struct.Struct('d')
colorsStruct = struct.Struct('30i')

memory = open('/proc/' + str(pid) + '/mem', 'rb')

def appptr():
    # read SuperHexagonApp pointer
    memory.seek(SuperHexagonApp)
    return pointerStruct.unpack(memory.read(8))[0]

def colors():
    # read the actual color data
    memory.seek(appptr() + 0x17ea * 8 + 0x2978)
    rgbs = colorsStruct.unpack(memory.read(120))
    return [(rgbs[i], rgbs[10+i], rgbs[20+i]) for i in range(10)]

def rotation():
    memory.seek(appptr() + 3 * 8 + 0xec)
    cos = intStruct.unpack(memory.read(4))[0]
    return cos


import pygame
pygame.init()


screen = pygame.display.set_mode((640, 480))

done = False
clock = pygame.time.Clock()
while not done:
    clock.tick(60)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
    print("APPTR", appptr())
    clrs = colors()
    for i, color in enumerate(clrs):
      screen.fill(color, (64*i, 0, 64, 480))
    rot = math.radians(rotation())
    pygame.draw.line(screen, (255, 0, 0), (320, 240), (320 + math.cos(rot) * 100, 240 + math.sin(rot) * 100), 5)
    pygame.display.flip()



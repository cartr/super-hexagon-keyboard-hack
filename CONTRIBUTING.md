I'm not really planning on supporting or maintaining this as a proper open-source project or anything; I wasn't even planning on releasing the code until people asked. If you're just looking to make your keyboard light up all cool without much effort on your part, this is probably not the repo for you.

That said, if you have any questions about how these programs work, or if you've done something cool with them, please feel free to shoot me an email at `carter.sande@duodecima.technology`.

#include <fcntl.h>
#include <math.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>

#include <stdbool.h>

pid_t superhex_pid = 0;
const off_t SuperHexagonApp = 12230672;

int colors[3][10];
int angle;
char filename_buffer[64];

bool superhexagon_init() {
    superhex_pid = fork();

    if (superhex_pid == 0) {
      // Run Super Hexagon
      chdir("/home/carter/.steam/steam/steamapps/common/Super Hexagon");
      putenv("LD_LIBRARY_PATH=x86_64");
      execlp("x86_64/superhexagon.x86_64", "x86_64/superhexagon.x86_64", NULL);
      exit(1);
    }

    if (superhex_pid <= 0) return false;

    if (snprintf(filename_buffer, 64, "/proc/%li/mem", (long)superhex_pid) < 0) return false;

    return true;
}

bool superhexagon_updatecolors() {
  int superhex_fd = open(filename_buffer, O_RDONLY);
  if (superhex_fd == -1) { return false; }
  unsigned long app_ptr;
  if (lseek(superhex_fd, SuperHexagonApp, SEEK_SET) == -1
      || read(superhex_fd, &app_ptr, sizeof(app_ptr)) == -1
      || lseek(superhex_fd, app_ptr + 0x17ea * 8 + 0x2978, SEEK_SET) == -1
      || read(superhex_fd, &colors[0][0], sizeof(colors)) == -1
      || lseek(superhex_fd, app_ptr + 0x3 * 8 + 0xec, SEEK_SET) == -1
      || read(superhex_fd, &angle, sizeof(angle)) == -1
     ) {
    close(superhex_fd);
    return false;
  }
  close(superhex_fd);
  return true;
}

bool superhexagon_dead() {
  return kill(superhex_pid, 0) == -1;
}

#ifndef SUPERHEX_NO_MAIN
int main() {
  if (superhexagon_init()) {
    printf("Init succeeded!\n");
    printf("PID: %li\n", (long)superhex_pid);
    printf("FD: %li\n", (long)superhex_fd);
    bool ever_succeeded = false;
    while (true) {
      sleep(1);
      if (superhexagon_updatecolors()) {
         printf("CLRS %i %i %i\n", colors[0][0], colors[1][0], colors[2][0]);
         ever_succeeded = true;
      } else if (ever_succeeded) {
        break;
      } else {
        perror("Color fail");
      }
    }
    perror("Colors failed");
  } else {
    perror("Init failed");
  }
}
#endif

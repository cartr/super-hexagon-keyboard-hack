#include <ckb-next/animation.h>
#include <fcntl.h>
#include <math.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#define SUPERHEX_NO_MAIN
#include "superhexagon.c"

void ckb_info(){
    // Plugin info
    CKB_NAME("Super Hexagon");
    CKB_VERSION("1.0");
    CKB_COPYRIGHT("2019", "Carter Sande");
    CKB_LICENSE("AGPL");
    CKB_GUID("{003322e1-7a8a-4301-9edb-06c88377091f}");
    CKB_DESCRIPTION("Synchronizes key lighting with the game Super Hexagon.");

    // Timing/input parameters
    CKB_KPMODE(CKB_KP_NONE);
    CKB_TIMEMODE(CKB_TIME_ABSOLUTE);
    CKB_REPEAT(FALSE);
    CKB_LIVEPARAMS(TRUE);

    // Presets
    CKB_PRESET_START("Super Hexagon");
    CKB_PRESET_END;
}

// KEYNAMES: left right w d

void ckb_parameter(ckb_runctx* context, const char* name, const char* value){
    // unused
}

void ckb_keypress(ckb_runctx* context, ckb_key* key, int x, int y, int state){
    // unused
}

void ckb_init(ckb_runctx* context) {
  superhexagon_init();
}
void ckb_start(ckb_runctx* context, int state) {
}

void ckb_time(ckb_runctx* context, double delta) {
  superhexagon_updatecolors();
}

int ckb_frame(ckb_runctx *context) {
  if (superhexagon_dead()) {
    return 1;
  }
  unsigned count = context->keycount;
  ckb_key *keys = context->keys;
  for (unsigned i =0; i < count; ++i) {
      keys[i].a = 255;
      float keyangle = atan2(keys[i].x - (context->width / 2.f), keys[i].y + context->height) + CKB_REAL_ANGLE(angle) + M_PI / 2;
      if (fmod(keyangle, 2 * M_PI / 3) < (M_PI / 3)) {
      keys[i].r = colors[0][2];
      keys[i].g = colors[1][2];
      keys[i].b = colors[2][2];
      } else {
      keys[i].r = colors[0][0];
      keys[i].g = colors[1][0];
      keys[i].b = colors[2][0];
      }
  }
  return 0;
}
